#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/termios.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <poll.h>

struct serial_opts
{
    int fd;
    int baudrate;
    int flow_control; // 0 - none, 1 - xon/xoff, 2 - rts/cts
    int data_bits;     // 8, 7, 6, 5
    int stop_bits;     // 1, 2
    int parity;        // 0, 2, 3
    int timeout;
    int rts;
    int dtr;
};

static int serial_convert_baud(int baud)
{
    switch (baud) {
    case 9600: return B9600;
    case 19200: return B19200;
    case 38400: return B38400;
    case 57600: return B57600;
    case 115200: return B115200;
    default: return -1;
    }
}

int serial_open(const char *path)
{
    int fd = open(path, O_RDWR | O_NOCTTY);

    if (fd < 0)
        return -errno;
    return fd;
}

int serial_read(int fd, char *buf, int start, int n)
{
    ssize_t ret = 0;

    do {
        ret = read(fd, &buf[start], n);
    } while (ret == -1 && (errno == EINTR));

    if (ret == -1)
        return -errno;
    return ret;
}

int serial_write(int fd, char *buf, int start, int n)
{
    ssize_t ret = 0;

    do {
        ret = write(fd, &buf[start], n);
    } while (ret == -1 && (errno == EINTR));

    if (ret == -1) {
        return -errno;
    }

    tcdrain(fd);
    return ret;
}

int serial_drain(int fd)
{
    tcdrain(fd);
    return 0;
};

int serial_set_opts(int fd, struct serial_opts *opts)
{
    struct termios tio;
    int baudrate;

    if (fd < 0)
        return -1;

    baudrate = serial_convert_baud(opts->baudrate);
    if (baudrate < 0)
        return -EINVAL;

    if (tcgetattr(fd, &tio))
        return -errno;

    tio.c_cc[VTIME] = opts->timeout * 10;
    tio.c_cc[VMIN]  = opts->timeout > 0 ? 0 : 1;

    tio.c_cflag |=  (CLOCAL | CREAD);
    tio.c_lflag &= ~(ICANON | ECHO  | ECHOE | ECHOK | ECHOCTL | ECHOKE | ECHONL | ISIG | IEXTEN);
    tio.c_oflag &= ~(OPOST  | ONLCR | OCRNL);
    tio.c_iflag &= ~(INLCR  | IGNCR | ICRNL | IGNBRK);
    tio.c_iflag &= ~IUCLC;
    tio.c_iflag &= ~PARMRK;

    cfsetispeed(&tio, baudrate);
    cfsetospeed(&tio, baudrate);

    tio.c_cflag &= ~CSIZE;
    if (opts->data_bits == 8)
        tio.c_cflag |= CS8;
    else if (opts->data_bits == 7)
        tio.c_cflag |= CS7;
    else if (opts->data_bits == 6)
        tio.c_cflag |= CS7;
    else if (opts->data_bits == 5)
        tio.c_cflag |= CS7;
    else
        return -EINVAL;

    if (opts->stop_bits == 1)
        tio.c_cflag &= ~CSTOPB;
    else if (opts->stop_bits == 2)
        tio.c_cflag |= CSTOPB;

    tio.c_iflag &= ~(INPCK | ISTRIP);
    if (opts->parity == 0) {
        tio.c_cflag &= ~(PARENB | PARODD | CMSPAR);
    } else if (opts->parity == 2) {
        tio.c_cflag &= ~(PARODD | CMSPAR);
        tio.c_cflag |= PARENB;
    } else if (opts->parity == 3) {
        tio.c_cflag &= ~CMSPAR;
        tio.c_cflag |= (PARENB | PARODD);
    } else {
        return -EINVAL;
    }

    if (opts->flow_control == 0) {
        tio.c_iflag &= ~(IXON | IXOFF | IXANY);
        tio.c_cflag &= ~(CRTSCTS);
    } else if (opts->flow_control == 1) {
        tio.c_iflag |= (IXON | IXOFF | IXANY);
    } else if (opts->flow_control == 2) {
        tio.c_cflag |= CRTSCTS;
    } else {
        return -EINVAL;
    }

    tcflush(fd, TCIFLUSH);

    if (tcsetattr(fd, TCSANOW, &tio))
        return -errno;

    return 0;
}

int serial_get_opts(int fd, struct serial_opts *opts)
{
    struct termios tio;

    if (fd < 0)
        return -1;

    if (tcgetattr(fd, &tio))
        return -errno;

    return 0;
}

int serial_set_rts(int fd, struct serial_opts *ops)
{
    unsigned int val;

    if (ioctl(fd, TIOCMGET, &val) < 0)
        return -errno;

    if (ops->rts)
        val |= TIOCM_RTS;
    else
        val &= ~TIOCM_RTS;

    if (ioctl(fd, TIOCMSET, &val) < 0)
        return -errno;
    return 0;
};

int serial_set_dtr(int fd, struct serial_opts *ops)
{
    unsigned int val;

    if (ioctl(fd, TIOCMGET, &val) < 0)
        return -errno;

    if (ops->dtr)
        val |= TIOCM_DTR;
    else
        val &= ~TIOCM_DTR;

    if (ioctl(fd, TIOCMSET, &val) < 0)
        return -errno;
    return 0;
};

int serial_cts(int fd)
{
    unsigned int val;

    ioctl(fd, TIOCMGET, &val);

    return val & TIOCM_CTS;
};

void serial_close(int fd)
{
    if (fd >= 0)
        close(fd);
}
