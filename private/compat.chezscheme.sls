#!r6rs
;; Taken from akku
(library (serial private compat)
  (export sleep-seconds)
  (import (chezscheme))

  (define (sleep-seconds t)
    (let* ((seconds (exact (truncate t)))
           (fraction (- t seconds))
           (nanoseconds (exact (round (* fraction 1e+9)))))
      (sleep (make-time 'time-duration nanoseconds seconds))))
)
