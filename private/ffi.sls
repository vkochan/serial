;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (serial private ffi)
  (export make-ffi-serial-opts
          ffi-serial-opts-baudrate
          ffi-serial-opts-baudrate-set!
          ffi-serial-opts-flow-control
          ffi-serial-opts-flow-control-set!
          ffi-serial-opts-data-bits
          ffi-serial-opts-data-bits-set!
          ffi-serial-opts-stop-bits
          ffi-serial-opts-stop-bits-set!
          ffi-serial-opts-parity
          ffi-serial-opts-parity-set!
          ffi-serial-opts-timeout
          ffi-serial-opts-timeout-set!
          ffi-serial-opts-rts
          ffi-serial-opts-dtr

          ffi-serial-open
          ffi-serial-read
          ffi-serial-write
          ffi-serial-close
          ffi-serial-drain
          ffi-apply-serial-opts
          ffi-apply-serial-rts
          ffi-serial-cts
          ffi-apply-serial-dtr)
  (import (rnrs)
          (pffi))

(define libserial
  (open-shared-object "libserial.so"))

(define-foreign-struct ffi-serial-opts
  (fields (int fd)
          (int baudrate)
	  (int flow-control)
	  (int data-bits)
	  (int stop-bits)
	  (int parity)
	  (int timeout)
	  (int rts)
	  (int dtr)))

(define (ffi-check-ret name ret)
  (when (not (= ret 0))
    (error name "Error while calling to ffi" ret) )
  ret)

(define ffi-serial-open (foreign-procedure libserial int serial_open (pointer)))
(define ffi-serial-set-opts (foreign-procedure libserial int serial_set_opts (int pointer)))
(define ffi-serial-get-opts (foreign-procedure libserial int serial_get_opts (int pointer)))
(define ffi-serial-close (foreign-procedure libserial void serial_close (int)))

(define serial-read (foreign-procedure libserial int serial_read (int pointer int int)))
(define serial-write (foreign-procedure libserial int serial_write (int pointer int int)))
(define serial-drain (foreign-procedure libserial int serial_drain (int)))
(define serial-rts-set (foreign-procedure libserial int serial_set_rts (int pointer)))
(define serial-dtr-set (foreign-procedure libserial int serial_set_dtr (int pointer)))
(define serial-cts (foreign-procedure libserial int serial_cts (int)))

(define (ffi-serial-read fd bv start n)
  (serial-read fd (bytevector->pointer bv) start n) )

(define (ffi-serial-write fd bv start n)
  (serial-write fd (bytevector->pointer bv) start n) )

(define (ffi-apply-serial-opts opts)
  (ffi-check-ret 'ffi-apply-serial-opts
                 (ffi-serial-set-opts (ffi-serial-opts-fd opts) (bytevector->pointer opts))))

(define (ffi-serial-drain opts)
  (ffi-check-ret 'ffi-serial-drain
                 (serial-drain (ffi-serial-opts-fd opts))))

(define (ffi-apply-serial-rts opts rts)
  (ffi-serial-opts-rts-set! opts rts)
  (ffi-check-ret 'ffi-apply-serial-rts
                 (serial-rts-set (ffi-serial-opts-fd opts) opts)))

(define (ffi-apply-serial-dtr opts dtr)
  (ffi-serial-opts-dtr-set! opts dtr)
  (ffi-check-ret 'ffi-apply-serial-dtr
                 (serial-dtr-set (ffi-serial-opts-fd opts) opts)))

(define (ffi-serial-cts opts)
  (serial-cts (ffi-serial-opts-fd opts)))

)
