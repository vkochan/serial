;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2025 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (private pty)
  (export open-pty
          open-raw-pty
          pty-fd
          pty-port
          pty-name
          drain-pty)
  (import (rnrs)
          (pffi)
          (private termios))

(define O_RDWR   #o2)
(define O_NOCTTY #o400)

(define EINTR 4)
(define EAGAIN 11)

(define (pointer->string p)
  (let loop ((i 0) (l '()))
    (let ((c (pointer-ref-c-uint8 p i)))
      (if (zero? c)
	  (list->string (reverse l))
	  (loop (+ i 1) (cons (integer->char c) l))))))

(define sys-open (foreign-procedure #f int open (pointer int)))
(define sys-grantpt (foreign-procedure #f int grantpt (int)))
(define sys-unlockpt (foreign-procedure #f int unlockpt (int)))
(define sys-ptsname (foreign-procedure #f pointer ptsname (int)))
(define sys-read (foreign-procedure #f int read (int pointer int)))
(define sys-write (foreign-procedure #f int write (int pointer int)))
(define sys-close (foreign-procedure #f int close (int)))

(define-foreign-variable #f int errno)

(define (grantpt fd)
  (let ([ret (sys-grantpt fd)])
    (when (eqv? ret -1)
      (error 'grantpt "Could not grant access to the terminal fd" fd))))

(define (unlockpt fd)
  (let ([ret (sys-unlockpt fd)])
    (when (eqv? ret -1)
      (error 'unlockpt "Could not unlock access to the terminal fd" fd))))

(define (ptsname fd)
  (let ([nm (sys-ptsname fd)])
    (when (eqv? 0 nm)
      (error 'ptsname "ptsname returned NULL" fd))
    (pointer->string nm)))

(define (handle-read-error fd fname err)
  (error 'pty-read "Error during reading from pty" fd fname err))

(define (read/bv fd fname bv start count)
  (let retry ()
    (let ([ret (sys-read fd (integer->pointer (fx+ (pointer->integer (bytevector->pointer bv)) start)) count)])
      (when (fx<=? ret -1)
        (cond ((eqv? errno EAGAIN)
               (retry))
              ((eqv? errno EINTR)
               (retry))
              (else
               (handle-read-error fd fname errno))))
    ret)))

(define (handle-write-error fd fname err)
  (error 'pty-write "Error during writing from pty" fd fname err))

(define (write/bv fd fname bv start count)
  (let retry ()
    (let ([ret (sys-write fd (integer->pointer (fx+ (pointer->integer (bytevector->pointer bv)) start)) count)])
      (when (fx<=? ret -1)
        (cond ((eqv? errno EAGAIN)
               (retry))
              ((eqv? errno EINTR)
               (retry))
              (else
               (handle-write-error fd fname errno))))
    ret)))

(define (make-binary-input/output-port-from-fd fd filename)
  (define (read! bv start count)
    (assert (fx<=? (fx+ start count) (bytevector-length bv)))
    (read/bv fd filename bv start count))
  (define (write! bv start count)
    (assert (fx<=? (fx+ start count) (bytevector-length bv)))
    (write/bv fd filename bv start count))
  (define get-position #f)
  (define set-position! #f)
  (define (close)
    (sys-close fd))
  (make-custom-binary-input/output-port
    filename read! write!  get-position set-position! close)
)

(define-record-type pty
  (fields fd name port))

;; TODO clear & reset SIGCHLD handler before & after grantpt & unlockpt
(define (open-pty)
  (let ([fd (sys-open "/dev/ptmx" (bitwise-ior O_RDWR O_NOCTTY))])
    (when (negative? fd)
      (error 'open-pty "Could not open /dev/ptmx"))
    (grantpt fd)
    (unlockpt fd)
    (make-pty fd (ptsname fd)
              (make-binary-input/output-port-from-fd fd "pty"))))

(define (open-raw-pty)
  (let ([tty (open-pty)])
    (termios-set-raw-mode (pty-fd tty))
    tty))

(define (drain-pty pt)
  (termios-drain (pty-fd pt)))

)
