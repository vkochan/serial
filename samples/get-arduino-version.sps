#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
        (serial)
        (serial private compat))

(when (< (length (command-line)) 2)
  (display "error: Missing argument: serial port file\n")
  (exit 1))

(define STK_SIGN_ON_MESSAGE "AVR STK")   ;; Sign on string for Cmnd_STK_GET_SIGN_ON

;; *****************[ STK Response constants ]***************************

(define Resp_STK_OK                #x10)
(define Resp_STK_FAILED            #x11)
(define Resp_STK_UNKNOWN           #x12)
(define Resp_STK_NODEVICE          #x13)
(define Resp_STK_INSYNC            #x14)
(define Resp_STK_NOSYNC            #x15)

;; *****************[ STK Special constants ]***************************

(define Sync_CRC_EOP               #x20)

;; *****************[ STK Command constants ]***************************

(define Cmnd_STK_GET_SYNC          #x30)
(define Cmnd_STK_GET_SIGN_ON       #x31)

(define Cmnd_STK_SET_PARAMETER     #x40)
(define Cmnd_STK_GET_PARAMETER     #x41)

;; *****************[ STK Parameter constants ]***************************

(define Parm_STK_HW_VER            #x80)
(define Parm_STK_SW_MAJOR          #x81)
(define Parm_STK_SW_MINOR          #x82)
(define Parm_STK_LEDS              #x83)
(define Parm_STK_VTARGET           #x84)
(define Parm_STK_VADJUST           #x85)
(define Parm_STK_OSC_PSCALE        #x86)
(define Parm_STK_OSC_CMATCH        #x87)
(define Parm_STK_RESET_DURATION    #x88)
(define Parm_STK_SCK_DURATION      #x89)

(define Parm_STK_BUFSIZEL           #x90)
(define Parm_STK_BUFSIZEH           #x91)
(define Parm_STK_DEVICE             #x92)
(define Parm_STK_PROGMODE           #x93)
(define Parm_STK_PARAMODE           #x94)
(define Parm_STK_POLLING            #x95)
(define Parm_STK_SELFTIMED          #x96)
(define Param_STK500_TOPCARD_DETECT #x98)

(define serial-path (list-ref (command-line) 1))

(define (sleep-milliseconds mls)
  (sleep-seconds (/ mls 1000)))

(define (sleep-microseconds mcs)
  (sleep-milliseconds (/ mcs 1000)))

(define-syntax command
  (syntax-rules ()
    [(_ v1 v* ...)
     (u8-list->bytevector (list v1 v* ...))]))

(define (send-cmd sp cmd)
  (let ((p (serial-port sp)))
    (put-bytevector p cmd)
    (flush-output-port p)
    (serial-drain sp)))

(define (send sp cmd)
  (put-bytevector (serial-port sp) cmd))

(define (recv sp)
  (get-u8 (serial-port sp)))

(define (stk500-get-param sp param)
  (define cmd (command Cmnd_STK_GET_PARAMETER param Sync_CRC_EOP))

  (let retry ((ntry 1))
    (send sp cmd)
    (let ((sync (recv sp)))
      (when (equal? sync Resp_STK_NOSYNC)
        (if (> ntry 33)
            (let ()
              (display "get-param: cannot sync with a device\n")
              #f)
            (if (not (stk500-get-sync sp))
                #f
                (retry (+ ntry 1)))))
      (if (not (equal? sync Resp_STK_INSYNC))
          (let ()
            (display "get-param: missing expected in-sync response: ")(display sync)(newline)
            #f)
          (let ((val (recv sp)))
            (let ((status (recv sp)))
              (cond
                ((equal? status Resp_STK_FAILED)
                 (display "get-param: command failed\n")
                 #f)
                ((equal? status Resp_STK_OK)
                 val)
                (else
                 (display "get-param: missing expected OK byte: ")(display status)(newline)
                 #f))))))))

(define (stk500-sync sp)
  (define cmd (command Cmnd_STK_GET_SYNC Sync_CRC_EOP))
  (define max-tries 10)

  (send sp cmd)
  (drain-input sp)
  (send sp cmd)
  (drain-input sp)
  
  (let loop ((ntry 0))
    (if (= ntry max-tries)
        (let ()
          (drain-input sp)
          #f)
        (let ()
          (display "Try #")(display ntry)(display " ...\n")
          (when (> ntry 0)
            ;; Arduino specific
            (serial-set-rts sp #t)
            (serial-set-dtr sp #t)
            (sleep-microseconds 100)
            (serial-set-rts sp #f)
            (serial-set-dtr sp #f)
            (sleep-milliseconds 20)
            (drain-input sp))

          (send-cmd sp cmd)
          (if (equal? (recv sp) Resp_STK_INSYNC)
              (if (equal? (recv sp) Resp_STK_OK)
                  #t
                  #f)
              (loop (+ ntry 1)))))))

(define (drain-input sp)
  (recv sp))

(let ([sp (open-serial serial-path '((baudrate 115200)) )])
  (serial-set-timeout sp 1)
  ;; Arduino specific
  (serial-set-rts sp #f)
  (serial-set-dtr sp #f)
  (sleep-milliseconds 250)
  (serial-set-rts sp #t)
  (serial-set-dtr sp #t)
  (sleep-microseconds 100)
  (serial-set-rts sp #f)
  (serial-set-dtr sp #f)
  (sleep-milliseconds 100)
  (drain-input sp)
  ;;

  (when (not (stk500-sync sp))
    (error 'main "Device not synced"))

  (display "Connected to the Adruino device\n")

  (let ((hw-ver (stk500-get-param sp Parm_STK_HW_VER))
        (sw-min (stk500-get-param sp Parm_STK_SW_MINOR))
        (sw-maj (stk500-get-param sp Parm_STK_SW_MAJOR)))
    (display "HW Version: ")(display hw-ver)(newline)
    (display "FW Version: ")(display sw-maj)(display ".")(display sw-min)(newline))
)
