#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
        (serial))

(when (< (length (command-line)) 2)
  (display "error: Missing argument: serial port file\n")
  (exit 1))

(define latin-1-transcoder (make-transcoder (latin-1-codec) 'crlf 'ignore))
(define serial-path (list-ref (command-line) 1))

(let ([sp (open-serial serial-path '((baudrate 115200)) )])
  (let loop ([tp (transcoded-port (serial-port sp) latin-1-transcoder)])
    (display (get-line tp))
    (loop tp) ))
