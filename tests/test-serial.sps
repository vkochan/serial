#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
	(srfi :64 testing)
	(serial)
	(private pty)
	(private termios))

(test-begin "serial basic tests")
(let* ([tty (open-raw-pty)]
       [device-name (pty-name tty)]
       [master (pty-port tty)]
       [sd (open-serial device-name)]
       [p (serial-port sd)])
  (test-assert p)
  (test-assert (port? p))

  (test-equal device-name (serial-name sd))

  (test-equal "default baudrate" 9600 (serial-baudrate sd))
  (test-equal "default data bits" 8 (serial-data-bits sd))
  (test-equal "default stop bits" 1 (serial-stop-bits sd))
  (test-equal "default flow control" 'none (serial-flow-control sd))
  (test-equal "default parity" 'none (serial-parity sd))
  (test-equal "default timeout" 0 (serial-timeout sd))

  (put-u8 master 123)
  (flush-output-port master)
  (test-equal "read from serial port" 123 (get-u8 p))

  (put-bytevector p #vu8(61 10))
  (flush-output-port p)
  (test-equal "write to serial port" #vu8(61 10) (get-bytevector-some master))

  (serial-set-timeout sd 1)
  (test-equal "read timeout" (eof-object) (get-bytevector-some p))

  (serial-set-baudrate sd 115200)
  (test-equal "set/get baudrate" 115200 (serial-baudrate sd))

  (serial-set-data-bits sd 5)
  (test-equal "set/get data bits" 5 (serial-data-bits sd))

  (serial-set-stop-bits sd 2)
  (test-equal "set/get stop bits" 2 (serial-stop-bits sd))

  (serial-set-parity sd 'even)
  (test-equal "set/get parity" 'even (serial-parity sd))

  (serial-set-flow-control sd 'rts/cts)
  (test-equal "set/get flow control" 'rts/cts (serial-flow-control sd))

  (close-port p)

  ;; test with options list
  (let* ([sd (open-serial device-name
                             '(
                               (flow-control xon/xoff)
                               (data-bits 7)
                               (stop-bits 2)
                               (baudrate 115200)
                               (parity odd)
                               (timeout 2)
                              )) ]
         [p (serial-port sd)])
    (test-equal "baudrate from list" 115200 (serial-baudrate sd))
    (test-equal "data bits from list" 7 (serial-data-bits sd))
    (test-equal "stop bits from list" 2 (serial-stop-bits sd))
    (test-equal "flow control from list" 'xon/xoff (serial-flow-control sd))
    (test-equal "parity from list" 'odd (serial-parity sd))

    (close-port p)
  )
)
(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
