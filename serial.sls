;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (serial)
  (export open-serial
          serial-port
          serial-baudrate
          serial-set-baudrate
          serial-data-bits
          serial-set-data-bits
          serial-stop-bits
          serial-set-stop-bits
          serial-flow-control
          serial-set-flow-control
          serial-parity
          serial-set-parity
          serial-name
          serial-timeout
          serial-set-timeout
          serial-drain
          serial-rts?
          serial-set-rts
          serial-dtr?
          serial-set-dtr
          serial-cts?)
  (import (rnrs)
          (serial private ffi))

(define-record-type serial-entry
  (fields name
          opts
          port))

(define (baudrate-valid? baudrate)
  (member baudrate  (list 9600 19200 38400 57600 115200)) )

(define (validate-baudrate baudrate)
  (when (not (baudrate-valid? baudrate))
    (error 'validate-baudrate "Baudrate is not valid" baudrate) ))

(define (validate-data-bits bits)
  (when (or (< bits 5) (> bits 8))
    (error 'validate-data-bits "Data bits is not valid" bits) ))

(define (validate-stop-bits bits)
  (when (or (< bits 1) (> bits 2))
    (error 'validate-stop-bits "Stop bits is not valid" bits) ))

(define (convert-flow-control ctl)
  (case ctl
    [(none)     0]
    [(xon/xoff) 1]
    [(rts/cts)  2]
    [else (error 'convert-flow-control "Flow-control is invalid" ctl)] ))

(define (convert-parity par)
  (case par
    [(none) 0]
    [(even) 2]
    [(odd)  3]
    [else (error 'convert-parity "Parity is invalid" par)] ))

(define (initialize-serial-opts opts)
  (let ([ret (ffi-apply-serial-opts opts)])
    (when (negative? ret)
      ;; TODO error conversion
      (error 'initialize-serial-opts "Could not apply options for serial port") )))

(define (fill-opts-from-alist opts alist)
  (let ([baudrate #f]
        [flow-control #f]
        [data-bits #f]
        [stop-bits #f]
        [parity #f]
        [timeout #f])
    (for-each
      (lambda (kv)
        (case (car kv)
          [(baudrate) (set! baudrate (cadr kv))]
          [(flow-control) (set! flow-control (cadr kv))]
          [(data-bits) (set! data-bits (cadr kv))]
          [(stop-bits) (set! stop-bits (cadr kv))]
          [(parity) (set! parity (cadr kv))]
          [(timeout) (set! timeout (cadr kv))]
          [else (error 'fill-opts-from-alist "Invalid option" (cadr kv))] ))
      alist)
    (when baudrate
      (validate-baudrate baudrate)
      (ffi-serial-opts-baudrate-set! opts baudrate) )
    (when flow-control
      (ffi-serial-opts-flow-control-set! opts (convert-flow-control flow-control)) )
    (when data-bits
      (validate-data-bits data-bits)
      (ffi-serial-opts-data-bits-set! opts data-bits) )
    (when stop-bits
      (validate-data-bits data-bits)
      (ffi-serial-opts-stop-bits-set! opts stop-bits) )
    (when parity
      (ffi-serial-opts-parity-set! opts (convert-parity parity)) )
    (when timeout
      (ffi-serial-opts-timeout-set! opts timeout) )
  )
)

(define open-serial
  (case-lambda
    [(path)
     (open-serial path '())]

    [(path/fd alist)
     (let ([fd (if (number? path/fd)
                   path/fd
                   (ffi-serial-open path/fd))]
           [path path/fd])
       (when (negative? fd)
         ;; TODO error conversion
         (error 'open-serial "Could not open serial port:" path))
       (let* ([opts (make-ffi-serial-opts fd 9600 0 8 1 0 0 0 0)])
         (fill-opts-from-alist opts alist)
         (initialize-serial-opts opts)
         (letrec ([port (make-custom-binary-input/output-port path
                    ;; r!
                    (lambda (bv start n)
                      (let ([ret (ffi-serial-read fd bv start n)])
                         (if (positive? ret)
                             ret 0)) )
                    ;; w!
                    (lambda (bv start n)
                      (let ([ret (ffi-serial-write fd bv start n)])
                         (if (positive? ret)
                             ret 0)) )
                    ;; gp
                    #f
                    ;; sp!
                    #f
                    ;; close
                    (lambda ()
                      (ffi-serial-close fd))
                  )])
           (make-serial-entry path opts port)
           )))]
  )
)

(define (serial-port dev)
  (serial-entry-port dev))

(define (serial-baudrate dev)
  (ffi-serial-opts-baudrate (serial-entry-opts dev)))

(define (serial-set-baudrate dev baudrate)
  (validate-baudrate baudrate)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-opts-baudrate-set! opts baudrate)
    (ffi-apply-serial-opts opts) ))

(define (serial-data-bits dev)
  (ffi-serial-opts-data-bits (serial-entry-opts dev)))

(define (serial-set-data-bits dev bits)
  (validate-data-bits bits)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-opts-data-bits-set! opts bits)
    (ffi-apply-serial-opts opts) ))

(define (serial-stop-bits dev)
  (ffi-serial-opts-stop-bits (serial-entry-opts dev)))

(define (serial-set-stop-bits dev bits)
  (validate-stop-bits bits)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-opts-stop-bits-set! opts bits)
    (ffi-apply-serial-opts opts) ))

(define (serial-flow-control dev)
  (case (ffi-serial-opts-flow-control (serial-entry-opts dev))
    [(0) 'none]
    [(1) 'xon/xoff]
    [(2) 'rts/cts]))

(define (serial-set-flow-control dev ctl)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-opts-flow-control-set! opts (convert-flow-control ctl))
    (ffi-apply-serial-opts opts) ))

(define (serial-parity dev)
  (case (ffi-serial-opts-parity (serial-entry-opts dev))
    [(0) 'none]
    [(2) 'even]
    [(3) 'odd]))

(define (serial-set-parity dev ctl)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-opts-parity-set! opts (convert-parity ctl))
    (ffi-apply-serial-opts opts) ))

(define (serial-name dev)
  (serial-entry-name dev))

(define (serial-timeout dev)
  (ffi-serial-opts-timeout (serial-entry-opts dev)))

(define (serial-set-timeout dev timeout)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-opts-timeout-set! opts timeout)
    (ffi-apply-serial-opts opts) ))

(define (serial-drain dev)
  (let ([opts (serial-entry-opts dev)])
    (ffi-serial-drain opts)))

(define (serial-rts? dev)
  (if (= (ffi-serial-opts-rts (serial-entry-opts dev)) 1)
      #t
      #f))

(define (serial-set-rts dev rts?)
  (let ([opts (serial-entry-opts dev)])
    (ffi-apply-serial-rts opts (if rts? 1 0)) ))

(define (serial-dtr? dev)
  (if (= (ffi-serial-opts-rts (serial-entry-opts dev)) 1)
      #t
      #f))

(define (serial-set-dtr dev dtr?)
  (let ([opts (serial-entry-opts dev)])
    (ffi-apply-serial-dtr opts (if dtr? 1 0)) ))

(define (serial-cts? dev)
  (if (= (ffi-serial-cts (serial-entry-opts dev)) 1)
      #t
      #f))

)
